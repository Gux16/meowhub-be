const jsonServer = require('json-server')
const path = require('path')
const middlewares = jsonServer.defaults()
const shortid = require('shortid')
const zip = new require('node-zip')();
const fs = new require('fs')

const server = jsonServer.create()
const router = jsonServer.router(path.join(__dirname, 'db.json'))

const db = router.db

const OSSClient = require('./oss');
const co = require('co');

// Set default middlewares (logger, static, cors and no-cache)
server.use(middlewares)

// 静态目录
server.get('/origin/:image', function (req, res) {
    co(function* () {
        var result = yield OSSClient.get(`/Data/DepponExpress-20171127/block_res/${req.params.image}`);
        res.send(result.content)
    }).catch(function (err) {
        console.log(err);
    });
})

server.use('/dist', function (req, res) {
    console.log(req._parsedOriginalUrl.pathname)
    co(function* () {
        var result = yield OSSClient.get(`/fe${req._parsedOriginalUrl.pathname}`);
        res.send(result.content)
    }).catch(function (err) {
        console.log(err);
    });
})

// Add custom routes before JSON Server router
server.get('/home', function (req, res) {
    // res.sendFile(path.join(__dirname, '../meowhub-fe/index.html'))
    co(function* () {
        var result = yield OSSClient.get(`/fe/index.html`);
        res.write(result.content.toString());
        res.end();
    }).catch(function (err) {
        console.log(err);
    });
})

// server.get('/put/:name', function (req, res) {
//     for (let item of db.get(req.params.name).value()) {
//         // let json_url = `/Volumes/Samsung_T5/kuaidi/dataset/tools/rect_json/${item.img_url.match(/\/origin\/(\S*)_vis.jpg/)[ 1 ]}.json`
//         // let data = fs.readFileSync(json_url, 'utf-8')
//         // let jsonObj = JSON.parse(data);
//         // item.blocks_configure = jsonObj
//         // console.log(json_url, 'success!')
//         // if (item.is_labeled) {
//         //     item.times = 1
//         // } else {
//         //     item.times = 0
//         // }
//         let img_url = `/Data/DepponExpress-20171127/block_res/${item.img_url.match(/\/origin\/(\S*)/)[1]}`
//         // console.log(sizeOf(img_url))
//     }
//     // db.get(req.params.name).write()
//     res.send(db.get(req.params.name).map('times').value())
// })

server.get('/add', function (req, res) {
    let title = 'users'
    let data = [{
        id: 1,
        name: 'guxiang',
        start: 1,
        current: 0,
        limit: 6
    }, {
        id: 2,
        name: 'gaoxiaolei',
        start: 1,
        current: 0,
        limit: 6
    }]
    console.log(db.has(title).value())
    if (db.has(title).value()) {
        db.set(title, data)
            .write()
    }
    res.send(db.get(title).value())
})

server.get('/api/getLabeled', function (req, res) {
    let count = 0
    for (let item of db.get('gux123').filter({is_labeled: true, is_correct: true}).value()) {
        let filename = item.img_url.match('\\/origin\\/(\\S*)_vis.jpg')[1] + '.json';
        zip.file(filename, JSON.stringify(item.blocks_configure));
        console.log(filename, count++)
    }
    let data = zip.generate({base64: false, compression: 'DEFLATE'});
    console.log(`成功!`)
    res.set({
        'Content-Disposition': `attachment;filename=labeled ${new Date().toLocaleString()}.zip`
    })
    res.send(new Buffer(data, 'binary'))
    // fs.writeFileSync('test.zip', data, 'binary');
})

server.get('/api/:name/count', function (req, res) {
    for (let item in req.query) {
        if (req.query[item] === 'true') req.query[item] = true;
        if (req.query[item] === 'false') req.query[item] = false;
    }
    res.send({
        count: db.get(req.params.name)
            .filter(req.query)
            .value()
            .length
    })
})

// To handle POST, PUT and PATCH you need to use a body-parser
// You can use the one used by JSON Server
server.use(jsonServer.bodyParser)
server.use((req, res, next) => {
    if (req.method === 'POST' && req.url === '/api/users') {
        req.body.id = shortid.generate()
        req.body.start = 0
        req.body.current = 0
        req.body.limit = 0
    }
    // Continue to JSON Server router
    next()
})

server.use((req, res, next) => {
    if (req.method === 'PATCH' && req.body.is_correct === true) {
        if (!db.has('labelHistory').value()) {
            db.set('labelHistory', []).write()
        } else {
            if (db.get('labelHistory').find({img: req.url.split('/')[3], user: req.headers.user}).value()) {
                let buf = db.get('labelHistory').find({img: req.url.split('/')[3], user: req.headers.user})
                buf.assign({time: buf.value().time + 1, lastModifiedAt: Date.now()}).write()
            } else {
                db.get('labelHistory')
                    .push({
                        id: shortid.generate(),
                        createdAt: Date.now(),
                        user: req.headers.user,
                        img: req.url.split('/')[3],
                        time: 1,
                        lastModifiedAt: Date.now()
                    })
                    .write()
            }
        }
    }
    // Continue to JSON Server router
    next()
})

// Use default router
server.use('/api', router)
server.listen(3001, () => {
    console.log('JSON Server is running')
})